package gr.hua.intranet;

import java.util.ArrayList;
import javax.validation.Valid;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import gr.hua.intranet.model.Admin;
import gr.hua.intranet.model.AdminDAO;
import gr.hua.intranet.model.User;


@Controller
public class AdminController {
 
 ArrayList<Admin> AdminList= new ArrayList<Admin>();
 
 ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
 
 AdminDAO adminDAO = ctx.getBean("AdminDAO", AdminDAO.class);
 
 @RequestMapping(value = "/nimda", method = RequestMethod.GET)
 public ModelAndView admin() {
    return new ModelAndView("admin", "command", new Admin());
 }
 
 @RequestMapping(value = "/checkAdmin", method = RequestMethod.POST)
  public String checkAdmin(@ModelAttribute("SpringWeb") Admin admin, ModelMap model) {
  
        Admin Admin = adminDAO.login(admin.getAdminName(), admin.getAdminPassword());
        
        if(Admin.isAdmin()){
         AdminList.add(Admin);
         return "redirect:/adminOptions";
        }
        
        return "redirect:/nimda";
 }
 
 @RequestMapping(value = "/adminOptions", method = RequestMethod.GET)
  public ModelAndView Options(Model model) {
  
   ArrayList<User> AllUsers = adminDAO.getAll();
   model.addAttribute("users", AllUsers);
   
   return new ModelAndView("adminOptions", "command", new User());
 }
 
 @RequestMapping(value = "/newUser", method = RequestMethod.GET)
 public String User(Model model) {
  User newUser = new User();
  model.addAttribute("newUser", newUser);
  return "newUser";
 }
 
 @RequestMapping(value = "/addOrUpdateUser", method = RequestMethod.POST)
 public String addUser(@ModelAttribute("newUser") @Valid User user, BindingResult result) {
  
   if (result.hasErrors()) {
             return "newUser";
         }
   
   adminDAO.CreateUser(user);
  
  return "redirect:/adminOptions";
 }

 @RequestMapping("/edit/{username}")
 public String editUser(@PathVariable("username") String username, Model model) {
  model.addAttribute("newUser", adminDAO.getUser(username));
  return "updateUser";
 }

 @RequestMapping("/update")
 public String editPerson(@ModelAttribute("updateUser") @Valid User user) {

  adminDAO.updateUser(user);
  return "redirect:/adminOptions";
 }
 
 @RequestMapping("/remove/{username}")
 public String removePerson(@PathVariable("username") String username) {

  adminDAO.deleteUser(username);
  return "redirect:/adminOptions";
 }
}