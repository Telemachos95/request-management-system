package gr.hua.intranet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import gr.hua.intranet.model.PDFfile;
import gr.hua.intranet.model.Request;
import gr.hua.intranet.model.RequestDAO;
import gr.hua.intranet.model.RequestDAOImpl;

@Controller
public class IssueController {
	ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
	RequestDAO requestDAO = ctx.getBean("requestDAO", RequestDAO.class);
	ArrayList<Request> requestList = new ArrayList<Request>();
	
	@RequestMapping(value = "/allRequests", method = RequestMethod.GET)
    public ModelAndView edit(Model model) {
		List<Request> requestList = requestDAO.getAll();
		model.addAttribute("requests", requestList);
		return new ModelAndView("request", "command", new Request());
    }
	
	@RequestMapping(value = "/getRequest", method = RequestMethod.POST)
    public String editRequest(@ModelAttribute("SpringWeb") Request request, ModelMap model) {
		requestList.clear();
		Request newRequest = requestDAO.getById(request.getRequestId());
		requestList.add(newRequest);
		return "redirect:/editRequest";
            
    }
	
	@RequestMapping(value = "/editRequest", method = RequestMethod.GET)
    public String index(Model model) {
            model.addAttribute("request", requestList);
            return "request1";
    }
	
	ArrayList<PDFfile> pdfList ;
	@RequestMapping(value = "/PdfFiles", method = RequestMethod.GET)
    public String PdfFiles(Model model) {
		 	pdfList = new ArrayList<PDFfile>();
			pdfList = requestDAO.getAllPDFS();
            model.addAttribute("pdfs", pdfList);
            return "request2";
    }
	
	@RequestMapping(value="/getpdf/{name}", method=RequestMethod.GET)
	public ResponseEntity<byte[]> getPDF(@ModelAttribute("SpringWeb") String name) {
				
		File file=null;
		
		for(int i=0;i<pdfList.size();i++){
			 file = new File(System.getProperty("user.home")+"\\Desktop\\Company_PDFS\\"+pdfList.get(i).getId()+".pdf");
		}
		
		byte[] data = new byte[(int) file.length()];
		
		FileInputStream fileInputStream;
		
		try {
			fileInputStream = new FileInputStream(file);
		    fileInputStream.read(data);
		    fileInputStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		 HttpHeaders headers = new HttpHeaders();
		 headers.setContentType(MediaType.parseMediaType("application/pdf"));
		 headers.setContentDispositionFormData(""+name, ""+name);
		 headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		 ResponseEntity<byte[]> response = new ResponseEntity<byte[]>(data, headers, HttpStatus.OK);
		 
		 return response;      
	}
	
	
	/**
	  * 
	  * acceptReq method used in order to accept this request.
	  */
	@RequestMapping(value = "/accept")
    public String requestAccept(Model model) {
		requestDAO.acceptReq(pdfList.get(0).getRequestID());
            return "redirect:/allRequests";
    }
	
	/**
	  * 
	  * declineReq method used in order to decline this request.
	  */
	@RequestMapping(value = "/decline/{requestId}")
    public String requestDecline(Model model) {
		requestDAO.declineReq(pdfList.get(0).getRequestID());
            return "redirect:/allRequests";
    }



	
}
