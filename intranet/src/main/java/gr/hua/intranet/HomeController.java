package gr.hua.intranet;

/**
 * 
 * Charlaftis Basileios it21370, Mitropoulos Ioannis it21338, Petsopoulos Tilemachos it21374
 */

import java.util.ArrayList;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import gr.hua.intranet.model.CompanyDAO;
import gr.hua.intranet.model.User;
import gr.hua.intranet.model.UserDAO;


@Controller
public class HomeController {

	/**
	  * 
	  * Add userDAO, companyDAO and user Beans from spring.xml.
	  */
	ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
	UserDAO userDAO = ctx.getBean("userDAO", UserDAO.class);
	CompanyDAO companyDAO = ctx.getBean("companyDAO",CompanyDAO.class);
	User user = ctx.getBean("user",User.class);
	ArrayList<User> userList = new ArrayList<User>();

	/**
	  * 
	  * This is Root Page,
	  * which go to user.jsp in order to login users.
	  */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView user() {
		  return new ModelAndView("user", "command", new User());
	}
	
	/**
	  * 
	  * Login method used to check from database if userName and userPass are correct,
	  * if they are correct go to index page, else go again to login form.
	  */
	@RequestMapping(value = "/checkUser", method = RequestMethod.POST)
    public String checkUser(@ModelAttribute("SpringWeb") User user, ModelMap model) {
            userList.clear();
			User newUser = userDAO.login(user.getUsername(), user.getUserpass());
            if(newUser.isUser()){
            	userList.add(newUser);
            	return "redirect:/index";
            }
            
            return "redirect:/";
    }
	
	/**
	  * 
	  * Go to index.jsp in order to see user's data and user's operations.
	  */
	@RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model) {
            model.addAttribute("user", userList);
            return "index";
    }
}
