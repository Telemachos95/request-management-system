package gr.hua.intranet;

/**
 * 
 * Charlaftis Basileios it21370, Mitropoulos Ioannis it21338, Petsopoulos Tilemachos it21374
 */

import java.util.ArrayList;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import gr.hua.intranet.model.Company;
import gr.hua.intranet.model.CompanyDAO;
import gr.hua.intranet.model.Deliver;
import gr.hua.intranet.model.DeliverDAO;
import gr.hua.intranet.model.User;
import gr.hua.intranet.model.UserDAO;

@Controller
public class SupportController {
	
	/**
	  * 
	  * Add userDAO, companyDAO, user, deliverDAO, company and deliver Beans from spring.xml.
	  */
	ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("spring.xml");
	UserDAO userDAO = ctx.getBean("userDAO", UserDAO.class);
	CompanyDAO companyDAO = ctx.getBean("companyDAO",CompanyDAO.class);
	User user = ctx.getBean("user",User.class);
	DeliverDAO deliverDAO = ctx.getBean("deliverDAO", DeliverDAO.class);
	Company company = ctx.getBean("company",Company.class);
	Deliver deliver = ctx.getBean("deliver", Deliver.class);
	ArrayList<Company> companyList = new ArrayList<Company>();
	
	/**
	  * 
	  * Go to adminOptions.jsp in order to give representative's adt and company's aam.
	  */
	@RequestMapping(value = "/req", method = RequestMethod.GET)
    public ModelAndView checkRep() {
		return new ModelAndView("company", "command", new Company());
    }
	
	/**
	  * 
	  * checkCompany method checks representative's adt and company's aam,
	  * if they are correct go to completeRequest,
	  * else go back again.
	  */
	Company newCompany;
	@RequestMapping(value = "/checkCompany", method = RequestMethod.POST)
    public String checkRep1(@ModelAttribute("SpringWeb") Company company, ModelMap model) {
            newCompany = companyDAO.checkCompany(company.getAdt(),company.getAam());
            if(newCompany.isCompany()){
            	companyList.add(newCompany);
            	return "redirect:/completeRequest";
            }            
            return "redirect:/req";
    }	
	
	/**
	  * 
	  * Go to register.jsp in order to check if company is correct in database.
	  */
	@RequestMapping(value = "/completeRequest", method = RequestMethod.GET)
    public String doRequest(Model model) {
		model.addAttribute("company",companyList);
		return "/register";
    }
	
	/**
	  * 
	  * Go to reqDone.jsp in order to choose duration for certification.
	  */
	@RequestMapping(value = "/completeReq", method = RequestMethod.POST)
    public ModelAndView doRequest1() {
		return new ModelAndView("reqDone", "command", new Deliver());
    }
	
	/**
	  * 
	  * completeCertification method writes in database request's data and returns the request's id.
	  */
	Deliver newDeliver;
	@RequestMapping(value = "/finish", method = RequestMethod.POST)
    public String doRep(@ModelAttribute("SpringWeb") Deliver deliver, ModelMap model) {
		newDeliver = deliverDAO.completeCertification(newCompany, deliver.getDuration()); 
        if(newDeliver.getReqID() >0){
        	model.addAttribute("register",newDeliver.getReqID());
           	return "/successReg";
        }
        return "redirect:/error";
    }	
	
	/**
	  * 
	  * Go to company2.jsp and give representative's adt and company's aam in order to take the certification.
	  */
	@RequestMapping(value = "/print", method = RequestMethod.GET)
    public ModelAndView receipt() {
		return new ModelAndView("company2", "command", new Company());
    }
	
	/**
	  * 
	  * checkCompany method checks representative's adt and company's aam,
	  * if they are correct go to deliver,
	  * else go back again.
	  */
	@RequestMapping(value = "/checkCompanyRec", method = RequestMethod.POST)
    public String checkRep2(@ModelAttribute("SpringWeb") Company company, ModelMap model) {
            newCompany = companyDAO.checkCompany(company.getAdt(),company.getAam());
            if(newCompany.isCompany()){
            	companyList.add(newCompany);
            	return "redirect:/deliver";
            }            
            return "redirect:/print";
    }	

	/**
	  * 
	  * Go to deliverReq.jsp in order to choose certification with given request id.
	  */
	@RequestMapping(value = "/deliver", method = RequestMethod.GET)
	public ModelAndView deliverRequest(){
		return new ModelAndView("deliverReq", "command", new Deliver());
	}
	
	/**
	  * 
	  * deliverCertification method updates database that certification with given request id is delivered and print it.
	  */
	@RequestMapping(value = "/reqIDafm", method = RequestMethod.POST)
	public String reqIDafm(@ModelAttribute("SpringWeb") Deliver deliver, ModelMap model){
		Deliver newDeliver = deliverDAO.deliverCertification(newCompany, deliver.getReqID());
		if(newDeliver.isDelivered()){
			return "redirect:/index";
		}
		return "redirect:/error";
	}
	
	@RequestMapping(value = "/error", method = RequestMethod.GET)
	public String error(){
		return "/error";
	}
}