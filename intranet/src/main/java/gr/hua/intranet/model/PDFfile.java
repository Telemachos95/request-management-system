package gr.hua.intranet.model;

import java.io.File;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PDFfile {
	
	private int Id;
	private String pdf;
	private int requestID;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getPdf() {
		return pdf;
	}
	public void setPdf(String pdf) {
		this.pdf = pdf;
	}
	public int getRequestID() {
		return requestID;
	}
	public void setRequestID(int requestID) {
		this.requestID = requestID;
	}
	
	
	
}