package gr.hua.intranet.model;

/**
 * 
 * Charlaftis Basileios it21370, Mitropoulos Ioannis it21338, Petsopoulos Tilemachos it21374
 */

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.beans.factory.annotation.Autowired;

public class DeliverDAOImpl implements DeliverDAO{
	
	private DataSource dataSource;
	private String FilePath;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	@Override
	public Deliver completeCertification(Company thisCompany, int duration) {
		String reqQuery = "insert into Requests (aam, brandname, repname, duration) values (?,?,?,?)";
		Deliver deliver = null;
		Connection con = null;
		PreparedStatement ps = null;
        int rs = 0;
        try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(reqQuery);
            ps.setInt(1, thisCompany.getAam());
            ps.setString(2, thisCompany.getBrandName());
            ps.setString(3, thisCompany.getRepName());
            ps.setInt(4, duration);            
            rs = ps.executeUpdate();
            if(rs!=0){            	
            	String idQuery = "select last_insert_id() as last_id from Requests";
            	Connection con2 = null;
            	PreparedStatement ps2 = null;
            	ResultSet rs2 = null;            	
            	try{   
            		con2 = dataSource.getConnection();
            		ps2 = con.prepareStatement(idQuery);        			
            		rs2 = ps2.executeQuery();
            		if(rs2.next()){
            			deliver = new Deliver();
            			deliver.setReqID(rs2.getInt("last_id"));
            		}
            		
            	}catch(SQLException e2){
            		e2.printStackTrace();
            	}finally{
            		try{
            			rs2.close();
            			ps2.close();
            			con2.close();
            		}catch(SQLException e2){
            			e2.printStackTrace();
            		}
            	}            	    
            }
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
		return deliver;
	}

	@Override
	public Deliver deliverCertification(Company thisCompany, int requestID) {
		Deliver deliver = new Deliver();
		String isDeliveredQuery = "update Requests set IsDelivered = true where RequestID = ? and aam = ? and IsDelivered = false and IsAccepted = true";
		Connection con = null;
		PreparedStatement ps = null;
		int rs = 0;
		try{
            con = dataSource.getConnection();
            ps = con.prepareStatement(isDeliveredQuery);
	            ps.setInt(1, requestID);
	            ps.setInt(2, thisCompany.getAam());
	            rs = ps.executeUpdate();	            
	            if(rs!=0){            	
	            	deliver.setDelivered(true);
	            }	            	                                 
        }catch(SQLException e){
            e.printStackTrace();
        }finally{
            try {
                ps.close();
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }		
		return deliver;
	}
}