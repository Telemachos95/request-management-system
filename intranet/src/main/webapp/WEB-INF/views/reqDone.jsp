<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Certificate Duration</title>
    <style>
         body {
 position: fixed;
  top: 50%;
  left: 50%;
  margin-top: -200px;
  margin-left: -150px;
  background: none repeat scroll 0 0 #111;
  color: #EEE;
}
div{background : none repeat scroll 0 0 #333333;height:150px;}
    </style>
</head>
<body>
<h2>Certificate Duration</h2>
<br>
<div>
<form:form method="POST" action="/intranet/finish">
   <table>
     <tr>
        <td><form:label path="duration">Duration (in months):</form:label></td>
        <td><form:input path="duration" /></td>
    </tr>   
    <tr>
        <td colspan="2">
            <input type="submit" value="Continue" style="float:right"/>
        </td>        
    </tr>
</table> 
</form:form>

<form action="/intranet/completeRequest">
<center><input type="submit" value="Back"></center>
</form>

<form action="/intranet/index">
<center><input type="submit" value="Cancel"></center>
</form>
</div>
</body>
</html>