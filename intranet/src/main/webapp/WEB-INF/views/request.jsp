<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>
<head>
        <title>All Requests</title>
         <style>
    body {
 position: fixed;
  top: 0%;
  left: 50%;
  margin-top: 20px;
  margin-left: -320px;
  background: none repeat scroll 0 0 #111;
  color: #EEE;
}
div{background : none repeat scroll 0 0 #333333;height:-200px;}
table, th, td {
      border: 1px solid black;
  }
    </style>
</head>
<body>
<h3>
       ALL REQUESTS
</h3>
<div>
<section>
<table>
        <tr>
        <th> Request ID </th>
        <th> AAM </th>
        <th> BrandName </th>
        <th> RepName </th>
        <th> Duration </th>
        <th> isAccepted </th>
        <th> isDelivered </th>
        </tr>
        <c:forEach items="${requests}" var="request">
  <tr>
    <td>${request.requestId}</td>
    <td>${request.aam}</td>
    <td>${request.brandName}</td>
    <td>${request.repName}</td>
    <td>${request.duration}</td>
    <td>${request.returnValueAcc}</td>   
    <td>${request.returnValueDel}</td> 
  </tr>
</c:forEach>
</table>
<br>
<form:form method="POST" action="/intranet/getRequest">
<center><form:label path="requestId">Provide Request ID:</form:label><center>
<form:input path="requestId" />
<input type="submit" value="Edit"/>
</form:form>
</section>
<br>
</div>
</html>