<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Authentication</title>
     <style>
       a.button {
    -webkit-appearance: button;
    -moz-appearance: button;
    appearance: button;
    text-decoration: none;
    color: initial;
}
     body {
 position: fixed;
  top: 50%;
  left: 50%;
  margin-top: -200px;
  margin-left: -150px;
  background: none repeat scroll 0 0 #111;
  color: #EEE;
}
div{background : none repeat scroll 0 0 #333333;height:150px;}
        </style>
</head>
<body>
<h2>Representative Authentication</h2>
<br>
<div>
<form:form method="POST" action="/intranet/checkCompanyRec">
   <table>
     <tr>
        <td><form:label path="adt">ADT:</form:label></td>
        <td><form:input path="adt" /></td>
    </tr>
    <tr>
        <td><form:label path="aam">AAM:</form:label></td>
        <td><form:input path="aam" /></td>
    </tr>
    <tr>
        <td colspan="2">
           <center><input type="submit" value="Authenticate" style="text-align:center;"/></center>
        </td>        
    </tr>
</table> 
</form:form>

<form action="/intranet/index">
 <center><input type="submit" value="Cancel"> </center>
</form>
</div>
</body>
</html>